//
//  ContentView.swift
//  BMI
//
//  Created by student on 3/11/20.
//  Copyright © 2020 student. All rights reserved.
//

import SwiftUI


//@State private var result = 0
//@State private var aws = ""
struct ContentView: View {
    
    @State private var weight: String = ""
    @State private var height: String = ""
    @State private var name: String = ""
    @State private var BMI:Double = 0.0
    @State private var aws: String = ""
    @State private var img: String = ""
    
    func clickbmi(weight:String,height:String){
        let w = (weight as NSString).doubleValue
        let h = (height as NSString).doubleValue
        BMI = (w/((h/100)*(h/100)))
    }
    
    func checkBMI(BMI:Double){
        if(BMI<18.5){
            img = "shrimp"
            aws = "กุ้งแห้ง"
        }else if(BMI<22.9){
            img = "cat"
            aws = "แมวน้อย"
        }else if(BMI<24.9){
            img = "pigma"
            aws = "ลูกหมู"
        }else if(BMI<29.9){
            img = "elephant"
            aws = "ช้างน้อย"
        }else{
            img = "godzilla"
            aws = "ก๊อตซิลล่า"
        }
        
    }
    
    var body: some View {
        NavigationView{
            VStack{
                Image("BMI").resizable().frame(width: 400, height: 150)
                VStack{
                    Text("คำนวณค่าน้ำหนัก")
                        .font(.title)
                    HStack{
                        Text("Name: ")
                        TextField("Enter your name", text: $name)
                            .padding(.horizontal)
                            .textFieldStyle(RoundedBorderTextFieldStyle())
                    }
                    VStack{
                        HStack {
                            Text("weight:")
                            TextField("Enter your name", text: $weight)
                                .padding(.horizontal)
                                .textFieldStyle(RoundedBorderTextFieldStyle())
                            
                        }
                        HStack {
                            Text("height:")
                            TextField("Enter your name", text: $height)
                                .padding(.horizontal)
                                .textFieldStyle(RoundedBorderTextFieldStyle())
                            
                        }
                    }
                }.offset(y: 10)
                
                VStack {
                    Button(action: {
                        self.clickbmi(weight:self.weight,height:self.height)
                        self.checkBMI(BMI: self.BMI)
                    }) {
                        
                        Text("Calculate BMI")
                            .padding()
                            .frame(width: 150, height: 50)
                            .border(Color.black)
                    }
                    .background(Color.blue)
                    .foregroundColor(Color.white)
                    
                    
                }
                .offset(y: 10)
                VStack {
                    Text("Name: \(name)")
                    Text("BMI: \(BMI, specifier: "%.2f")")
                    Image("\(img)")
                    Text("ค่าที่ได้: \(aws)")
                }
            .offset(y: 15)
                
            }
            .navigationBarTitle("BMI")
        }
    }
}

//struct PrimaryLabel:ViewModifier {
//    func body(content:Content) -> some View {
//        content
//        .padding()
//        .background(Color.blue)
//        .foregroundColor(Color.white)
//        .font(.largeTitle)
//    }
//}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
